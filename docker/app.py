from flask import Flask
from flask_restful import Resource, Api
import requests
import json

app = Flask(__name__)
api = Api(app)


class HelloWorld(Resource):
    def get(self):
        response = requests.get('REPLACE_ENDPOINT')
        return {
            'statusCode': 200,
            'body': json.dumps(response.json())
        }


api.add_resource(HelloWorld, '/')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
