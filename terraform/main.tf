module "ec2_instance" {
  source = "./modules/ec2"
}
module "lambda_metrics" {
  source         = "./modules/lambda_metrics"
  ec_instance_id = module.ec2_instance.ec_instance_arn
  bucket_name    = var.bucket_name
}
module "es" {
  source         = "./modules/es"
  domain_name = var.domain_name
  bucket_name    = var.bucket_name
  bucket_arn = module.lambda_metrics.bucket_arn
}
module "api_gateway" {
  source         = "./modules/api_gateway"
  bucket_name    = var.bucket_name
  es_endpoint = module.es.es_endpoint

}
module "docker_rest" {
  source         = "./modules/docker_rest"
  base_url = module.api_gateway.base_url
} 