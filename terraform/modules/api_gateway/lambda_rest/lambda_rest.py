from __future__ import print_function
from pprint import pprint
import boto3
import json
from elasticsearch import Elasticsearch, RequestsHttpConnection
from datetime import datetime
import urllib
import os
import logging


def lambda_handler(event, context):

    metrics_list = ["CPUUtilization", "DiskWriteOps", "DiskReadOps", "DiskWriteBytes",
                    "DiskReadBytes", "NetworkOut", "NetworkIn", "NetworkPacketsOut", "NetworkPacketsIn"]
    key_prefix = str(datetime.now().year) + "-" + \
        str(datetime.now().month) + "-" + str(datetime.now().day)
    bucket_name = os.environ['bucket_name']
    endpoint = os.environ['es_endpoint']
    esClient = connectES(endpoint)
    output = queryES(metrics_list=metrics_list, esClient=esClient)

    return {
        'statusCode': 200,
        'body': json.dumps(output)
    }


def connectES(esEndPoint):
    """Returns ES client"""
    try:
        esClient = Elasticsearch(
            hosts=[{'host': esEndPoint, 'port': 443}],
            use_ssl=True,
            verify_certs=True,
            connection_class=RequestsHttpConnection)
        logging.info("Connected to ES domain")
        return esClient
    except Exception as E:
        logging.info("Unable to connect to ES domain")
        logging.error(E)


def queryES(metrics_list, esClient):
    """Returns the metrics from ES as a dictionary with each key being a metric and follwed by the query result"""
    output = {}

    try:
        for metric in metrics_list:
            index = metric.lower()
            response = esClient.search(index=index, body={
                "query": {
                    "bool": {
                        "filter": {
                            "range": {
                                "timestamp": {
                                    "gte": "now-24h",
                                    "lte": "now"
                                }
                            }
                        }
                    }
                },
                "sort":
                {
                    "timestamp": {
                        "order": "asc"
                    }
                }
            })
            output[metric] = response['hits']['hits']
        logging.info("Data loaded from ES")
    except Exception as E:
        logging.error("Could not query ES")
        logging.error(E)

    return output
