resource "aws_lambda_function" "lambda_function_rest" {
  depends_on    = [aws_iam_role_policy_attachment.lambda_rest]
  filename      = "./modules/api_gateway/lambda_rest.zip"
  function_name = "lambda_rest"
  role          = aws_iam_role.iam_for_lambda_rest.arn
  handler       = "lambda_rest.lambda_handler"
  timeout = 30


  source_code_hash = filebase64sha256("./modules/api_gateway/lambda_rest.zip")

  runtime = "python3.8"

  environment {
    variables = {
      bucket_name = var.bucket_name
      es_endpoint = var.es_endpoint
    }
  }
}
resource "aws_lambda_permission" "apigw" {
   statement_id  = "AllowAPIGatewayInvoke"
   action        = "lambda:InvokeFunction"
   function_name = aws_lambda_function.lambda_function_rest.function_name
   principal     = "apigateway.amazonaws.com"

   # The "/*/*" portion grants access from any method on any resource
   # within the API Gateway REST API.
   source_arn = "${aws_api_gateway_rest_api.rest_api.execution_arn}/*/*"
 }

resource "aws_iam_role" "iam_for_lambda_rest" {
  name = "iam_for_lambda_rest"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
resource "aws_iam_policy" "lambda_rest_policy" {
  name        = "lambda_rest_policy"
  path        = "/"
  description = "IAM policy for the REST lambda function"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "autoscaling:Describe*",
                "cloudwatch:Describe*",
                "cloudwatch:Get*",
                "cloudwatch:List*",
                "logs:Get*",
                "logs:List*",
                "logs:Describe*",
                "logs:TestMetricFilter",
                "logs:FilterLogEvents",
                "sns:Get*",
                "sns:List*"
            ],
            "Effect": "Allow",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "logs:CreateLogGroup",
            "Resource": "arn:aws:logs:*:*:*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": [
                "arn:aws:logs:*:*:log-group:/aws/lambda/EC2_Metrics:*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "es:*"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "lambda_rest" {
  role       = aws_iam_role.iam_for_lambda_rest.name
  policy_arn = aws_iam_policy.lambda_rest_policy.arn
}
