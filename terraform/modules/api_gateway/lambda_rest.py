from __future__ import print_function
from pprint import pprint
import boto3
import json
from elasticsearch import Elasticsearch, RequestsHttpConnection
from datetime import datetime
import urllib
import os


def lambda_handler(event, context):

    metrics_list = ["CPUUtilization", "DiskWriteOps", "DiskReadOps", "DiskWriteBytes",
                    "DiskReadBytes", "NetworkOut", "NetworkIn", "NetworkPacketsOut", "NetworkPacketsIn"]
    key_prefix = str(datetime.now().year) + "-" + \
        str(datetime.now().month) + "-" + str(datetime.now().day)
    bucket_name = os.environ['bucket_name']
    endpoint = os.environ['endpoint']
    esClient = connectES(endpoint)

    for metric in metrics_list:
        index = metric.lower()
        response = esClient.search(index=index, body={
            "query": {
                "bool": {
                    "filter": {
                        "range": {
                            "@timestamp": {
                                "gte": "now-24h",
                                "lte": "now"
                            }
                        }
                    }
                }
            },
            "sort":
            {
                "timestamp": {
                    "order": "asc"
                }
            }
        })

    return {
        'statusCode': 200,
        'body': json.dumps('Metrics successfully indexed')
    }


def connectES(esEndPoint):
    try:
        esClient = Elasticsearch(
            hosts=[{'host': esEndPoint, 'port': 443}],
            use_ssl=True,
            verify_certs=True,
            connection_class=RequestsHttpConnection)
        print("Connected to ES service")
        return esClient
    except Exception as E:
        print("Unable to connect to ES service")
        raise E
