variable "image_id" {
  type    = string
  default = "ami-0a887e401f7654935"
}
variable "base_url" {
  
}
variable "policy_ssm_arn" {
  type = string
  default = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
}
