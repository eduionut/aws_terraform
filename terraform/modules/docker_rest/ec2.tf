resource "aws_instance" "docker_ec2" {
  
  depends_on = [aws_iam_role_policy_attachment.docker_ec2]
  ami                  = var.image_id
  instance_type        = "t2.micro"
  iam_instance_profile = aws_iam_instance_profile.docker_profile.name
	user_data = <<EOF
#! /bin/bash
sudo yum -y update
sudo yum install git -y
sudo yum install -y docker
sudo service docker start
git clone https://eduionut@bitbucket.org/eduionut/aws_terraform.git
sed -i 's~REPLACE_ENDPOINT~${var.base_url}~g' aws_terraform/docker/app.py
docker build -t my_docker_flask:latest aws_terraform/docker/.
docker run -d -p 5000:5000 my_docker_flask:latest
EOF
}

resource "aws_iam_instance_profile" "docker_profile" {
  name = "docker_profile"
  role = "${aws_iam_role.iam_for_docker_ec2.name}"
}
resource "aws_iam_role" "iam_for_docker_ec2" {
  name = "iam_for_docker_ec2"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "docker_ec2" {
  role       = aws_iam_role.iam_for_docker_ec2.name
  policy_arn = var.policy_ssm_arn
}