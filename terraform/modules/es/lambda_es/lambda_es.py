from __future__ import print_function
from pprint import pprint
import boto3
import json
from elasticsearch import Elasticsearch, RequestsHttpConnection
from datetime import datetime
import urllib
import os
import logging



def lambda_handler(event, context):

    indexDoc = {
    "settings" : {
        "number_of_shards": 1,
        "number_of_replicas": 0
      }
    }
    metrics_list = ["CPUUtilization", "DiskWriteOps", "DiskReadOps", "DiskWriteBytes",
                    "DiskReadBytes", "NetworkOut", "NetworkIn", "NetworkPacketsOut", "NetworkPacketsIn"]
    key_prefix = str(datetime.now().year) + "-" + str(datetime.now().month) + "-" + str(datetime.now().day)   
    bucket_name = os.environ['bucket_name']
    endpoint = os.environ['endpoint']
    esClient = connectES(endpoint)
    
    for metric in metrics_list:
        key = str(metric) + "/" + key_prefix
        data = get_data(bucket_name, key)
        index = data['Label'].lower()
        createIndex(esClient,index, indexDoc = indexDoc)
        for datapoint in data['Datapoints']:
            indexDocElement(esClient,datapoint,index)
    
    return {
        'statusCode': 200,
        'body': json.dumps('Metrics successfully indexed')
    }

def connectES(esEndPoint):
    """Returns ES client"""
    try:
        esClient = Elasticsearch(
            hosts=[{'host': esEndPoint, 'port': 443}],
            use_ssl=True,
            verify_certs=True,
            connection_class=RequestsHttpConnection)
        logging.info("Connected to ES domain")
        return esClient
    except Exception as E:
        logging.info("Unable to connect to ES domain")
        logging.error(E)

def createIndex(esClient, index, indexDoc):
    try:
        res = esClient.indices.exists(index)
        if res is False:
            esClient.indices.create(index, body=indexDoc)
            logging.info("Index" + str(index) + "created")
        return 1
    except Exception as E:
        logging.error("Unable to Create Index " + str(index))
        logging.error(E)

def indexDocElement(esClient,data,index):
    try:
        timestamp = datetime.strptime(data['Timestamp'], '%Y-%m-%d %X+00:00')
        value = data['Maximum']
        unit = data['Unit']
        retval = esClient.index(index=index, body={
                'timestamp': timestamp,
                'value': value,
                'unit': unit
        })
        logging.info("Data indexed in " + str(index))
    except Exception as E:
        logging.error(E)

def get_data(bucket, key):
    try:
        client = boto3.client('s3')
        data = client.get_object(Bucket=bucket,Key=key)['Body'].read().decode('utf-8')
        data = json.loads(data)
        logging.info("Data loaded from S3")
        return data
    except Exception as E:
        logging.error("Unable to get data from S3")
        logging.error(E)