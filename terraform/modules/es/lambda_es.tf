resource "aws_lambda_function" "lambda_function_es" {
  depends_on    = [aws_iam_role_policy_attachment.lambda_es]
  filename      = "./modules/es/lambda_es.zip"
  function_name = "lambda_es"
  role          = aws_iam_role.iam_for_lambda_es.arn
  handler       = "lambda_es.lambda_handler"
  timeout = 30


  source_code_hash = filebase64sha256("./modules/es/lambda_es.zip")

  runtime = "python3.8"

  environment {
    variables = {
      endpoint = aws_elasticsearch_domain.metrics_es.endpoint
      bucket_name = var.bucket_name
    }
  }
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = var.bucket_name

  lambda_function {
    lambda_function_arn = aws_lambda_function.lambda_function_es.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "CPUUtilization/"
  }
}


resource "aws_lambda_permission" "allow_s3" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_function_es.function_name
  principal     = "s3.amazonaws.com"
  source_arn    = var.bucket_arn
}

resource "aws_iam_role" "iam_for_lambda_es" {
  name = "iam_for_lambda_es"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
resource "aws_iam_policy" "lambda_es_policy" {
  name        = "lambda_es_policy"
  path        = "/"
  description = "IAM policy for the lambda function indexing data in ES"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "autoscaling:Describe*",
                "cloudwatch:Describe*",
                "cloudwatch:Get*",
                "cloudwatch:List*",
                "logs:Get*",
                "logs:List*",
                "logs:Describe*",
                "logs:TestMetricFilter",
                "logs:FilterLogEvents",
                "sns:Get*",
                "sns:List*"
            ],
            "Effect": "Allow",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "logs:CreateLogGroup",
            "Resource": "arn:aws:logs:*:*:*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": [
                "arn:aws:logs:*:*:log-group:/aws/lambda/EC2_Metrics:*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "es:*"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "lambda_es" {
  role       = aws_iam_role.iam_for_lambda_es.name
  policy_arn = aws_iam_policy.lambda_es_policy.arn
}
