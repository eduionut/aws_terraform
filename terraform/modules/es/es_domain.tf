resource "aws_elasticsearch_domain" "metrics_es" {
  domain_name           = var.domain_name
  elasticsearch_version = "7.1"

  cluster_config {
    instance_type  = "t2.small.elasticsearch"
    instance_count = "1"
  }
  ebs_options {
    ebs_enabled = "true"
    volume_size = "10"
  }
  snapshot_options {
    automated_snapshot_start_hour = 23
  }
  access_policies = <<POLICY
  {
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "*"
        ]
      },
      "Action": [
        "es:*"
      ],
      "Resource": "arn:aws:es:*:*:domain/${var.domain_name}/*"
    }
  ]
}
POLICY
}
