output "es_endpoint" {
  value = aws_elasticsearch_domain.metrics_es.endpoint
}
