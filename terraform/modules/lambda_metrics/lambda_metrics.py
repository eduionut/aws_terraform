import json
import boto3
from datetime import datetime, timedelta
import os


def lambda_handler(event, context):

    last24h = datetime.now() - timedelta(hours=24)
    metrics_list = ["CPUUtilization", "DiskWriteOps", "DiskReadOps", "DiskWriteBytes",
                    "DiskReadBytes", "NetworkOut", "NetworkIn", "NetworkPacketsOut", "NetworkPacketsIn"]
    instance_id = os.environ['instance_id']
    bucket_name = os.environ['bucket_name']

    for metric in metrics_list:
        response = get_metrics(metric, instance_id,
                               frequency=3600, last24h=last24h)
        write_metrics_s3(bucket_name, metric=metric,
                         response=response, last24h=last24h)

    return {
        'statusCode': 200,
        'body': json.dumps('Metrics successfully uploaded')
    }


def get_metrics(metric, instance_id, frequency, last24h):
    try:
        client = boto3.client('cloudwatch')
        response = client.get_metric_statistics(
            Namespace='AWS/EC2',
            MetricName=metric,
            Dimensions=[
                {
                    'Name': 'InstanceId',
                    'Value': instance_id
                },
            ],
            StartTime=datetime(last24h.year, last24h.month, last24h.day),
            EndTime=datetime(last24h.year, last24h.month,
                             datetime.now().day + 1),
            Period=frequency,
            Statistics=[
                'Maximum'
            ]
        )
        return response
    except Exception as E:
        print("Unable to get metrics from cloudwatch")
        raise E


def write_metrics_s3(bucket_name, metric, response, last24h):
    try:
        s3 = boto3.resource('s3')
        obj = s3.Object(bucket_name, str(metric) + "/" + str(datetime.now().year) +
                        "-" + str(datetime.now().month) + "-" + str(datetime.now().day))
        obj.put(Body=(json.dumps(response, default=myconverter)))
    except Exception as E:
        print("Unable to write metrics in S3")
        raise E


def myconverter(o):
    if isinstance(o, datetime):
        return o.__str__()
