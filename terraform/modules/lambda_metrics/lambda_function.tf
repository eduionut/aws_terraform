resource "aws_lambda_function" "lambda_function_metrics" {
  depends_on    = [aws_iam_role_policy_attachment.lambda_logs]
  filename      = "./modules/lambda_metrics/lambda_metrics.zip"
  function_name = "lambda_metrics"
  role          = aws_iam_role.iam_for_lambda_metrics.arn
  handler       = "lambda_metrics.lambda_handler"
  timeout = 30


  source_code_hash = filebase64sha256("./modules/lambda_metrics/lambda_metrics.zip")

  runtime = "python3.8"

  environment {
    variables = {
      instance_id = var.ec_instance_id
      bucket_name = var.bucket_name
    }
  }
}

resource "aws_cloudwatch_event_rule" "every_day" {
  name                = "every_day"
  description         = "Once per day"
  schedule_expression = "rate(1440 minutes)"
}

resource "aws_cloudwatch_event_target" "check_every_day" {
  rule = aws_cloudwatch_event_rule.every_day.name
  arn  = aws_lambda_function.lambda_function_metrics.arn
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_check_foo" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_function_metrics.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.every_day.arn
}

resource "aws_s3_bucket" "metrics_bucket" {
  bucket = var.bucket_name
  acl    = "private"
}