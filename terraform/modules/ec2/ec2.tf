resource "aws_instance" "metrics_ec2" {

  ami                  = var.image_id
  instance_type        = "t2.micro"
  iam_instance_profile = aws_iam_instance_profile.metrics_ec2_profile.name

}

resource "aws_iam_instance_profile" "metrics_ec2_profile" {
  name = "metrics_ec2_profile"
  role = "${aws_iam_role.iam_for_metrics_ec2.name}"
}
resource "aws_iam_role" "iam_for_metrics_ec2" {
  name = "iam_for_metrics_ec2"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "metrics_ec2" {
  role       = aws_iam_role.iam_for_metrics_ec2.name
  policy_arn = var.policy_ssm_arn
}